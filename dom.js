var form= document.getElementById("addform");
var items= document.getElementById("mylist");




form.addEventListener("submit", additems);

function additems(e)
{

    e.preventDefault();

    var li= document.createElement("li");
    var input= document.getElementById("enterme").value; 

    if(!input)  //input is null so dont add anything
    return;
    

    li.appendChild(document.createTextNode(input));

    var btn =document.createElement("button");
    btn.appendChild(document.createTextNode("X"));

    li.appendChild(btn);

    items.appendChild(li);

    

}

//----------------------------------------------------------------------------


items.addEventListener("click",removeitems);

function removeitems(e)
{
   

    if(confirm("Are you sure?"))
    items.removeChild(e.target.parentNode);

}


//------------------------------------------------------------------------------

var filter= document.getElementById("filter");

filter.addEventListener("keyup", filteritems);



function filteritems(e)
{
  //Method-1

  var text= document.getElementById("filter").value.toLowerCase();

  //now i want to iterate over list items

  var obj= document.getElementById("mylist");

  //obj(object)= {id="mylist", children:{0: li(object), 1: li(object), 2: li(object), 3: li(object)}, lastElementChild: li(object)}

  var object= obj.children;


  Array.from(object).forEach((item)=>{

    var itemName = item.firstChild.textContent;       
  
    if(itemName.toLowerCase().indexOf(text) != -1)
    {
     item.style.display = 'block';
    } 
    else
     {
    item.style.display = 'none';
     }

})


 /*  Method-2
    
    var text= e.target.value.toLowerCase();
      console.log(text);

      var obj= items.getElementsByTagName("li");

      //obj(object)={0: li(object), 1: li(object), 2: li(object), 3: li(object)}
  
      Array.from(obj).forEach((item)=>{
  
    //item = li(object) ={firstchild: Item-1, lastChild: button, id:" "}

         var itemName = item.firstChild.textContent;       
  
              if(itemName.toLowerCase().indexOf(text) != -1)
              {
               item.style.display = 'block';
              } 
              else
               {
              item.style.display = 'none';
               }
  
  })   */

  


 }
















